<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\UserController as AdminUserController;
use App\Http\Controllers\Admin\StudentController as AdminStudentController;
use App\Http\Controllers\Admin\ComplaintController as AdminComplaintController;
use App\Http\Controllers\Admin\ResponseController as AdminResponseController;
use App\Http\Controllers\Admin\SummaryController as AdminSummaryController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\Staff\UserController as StaffUserController;
use App\Http\Controllers\Staff\StudentController as StaffStudentController;
use App\Http\Controllers\Staff\ComplaintController as StaffComplaintController;
use App\Http\Controllers\Staff\ResponseController as StaffResponseController;

use App\Http\Controllers\Student\ComplaintController as StudentComplaintController;
use App\Http\Controllers\Student\ResponseController as StudentResponseController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {
    Route::middleware(['level:admin'])->group(function () {
        Route::get('/admin', function () {
            return view('admin.home');
        });
        Route::resource('/admin/users', AdminUserController::class);
        Route::resource('/admin/students', AdminStudentController::class);
        Route::resource('/admin/complaints', AdminComplaintController::class);
        Route::resource('/admin/responses', AdminResponseController::class);
        Route::get('/admin/response-report', [AdminSummaryController::class, 'responseReport']);
        Route::get('/admin/complaint-report', [AdminSummaryController::class, 'complaintReport']);
    });
    Route::middleware(['level:staff'])->group(function () {
        Route::get('/staff', function () {
            return view('staff.home');
        });
        Route::resource('/staff/users', StaffUserController::class);
        Route::resource('/staff/students', StaffStudentController::class);
        Route::resource('/staff/complaints', StaffComplaintController::class);
        Route::resource('/staff/responses', StaffResponseController::class);
    });
    Route::middleware(['level:student'])->group(function () {
        Route::get('/student', function () {
            return view('student.home');
        });
        
        Route::resource('/student/complaints', StudentComplaintController::class);
        Route::resource('/student/responses', StudentResponseController::class);
    });
});

Route::get('/home', function () {
    return view('home');
});

Route::get('/bullyinginformation', function () {
    return view('bullyinginformation');
});

Route::get('/efekbullying', function () {
    return view('efekbullying');
});

Route::get('/sanksibullying', function () {
    return view('sanksibullying');
});

Route::get('/contactus', function () {
    return view('contactus');
});

Route::get('/bullyingisreal', function () {
    return view('bullyingisreal');
});

Route::get('/aboutus', function () {
    return view('aboutus');
});

Route::get('/contactus', function () {
    return view('contactus');
});

Route::get('/statistik', function () {
    return view('statistik');
});

Route::get('/login', [LoginController::class, 'show'])->name('login');
Route::post('/login', [LoginController::class, 'check']);
Route::get('/logout', [LoginController::class, 'logout']);
