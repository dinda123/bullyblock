<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplaints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaints', function (Blueprint $table) {
            $table->id();
            $table->string('victim_name', 35);
            $table->date('incident_date');
            $table->string('location', 500);
            $table->enum('type_bullying', ['verbal bullying', 'social bullying', 'physical bullying', 'cyber bullying', 'sexual bullying']);
            $table->text('report_content');
            $table->text('image');
            $table->string('addresed_to', 35);
            $table->enum('status', ['not verified', 'unprocessed', 'rejected', 'process', 'finish'])->default('not verified');
            $table->foreignId('reporter_id')->constrained('students', 'id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaints');
    }
}
