@extends('app')

@section('content')
    <div class="container">
        <h1>Add new complaint</h1>
        <form action="/admin/complaints" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row flex-column">
                <div class="col-12 mb-12">
                    <label for="victim_name" class="form-label">Victim_name</label>
                    <input type="text" class="form-control" id="victim_name" name="victim_name">
                </div>
            

            
                <div class="col-12 mb-12">
                    <label for="incident_date" class="form-label">Incident_date</label>
                    <input type="date" class="form-control" id="incident_date" name="incident_date">
                </div>
            

            
                <div class="col-12 mb-12">
                    <label for="location" class="form-label">Location</label>
                    <input type="text" class="form-control" id="location" name="location">
                </div>
            

            <div class="col-12 mb-12">
                <label class="form-label">Type_bullying</label>
                <select name="type_bullying" class="form-select">
                    @foreach (['verbal bullying', 'physical bullying', 'cyber bullying', 'social bullying', 'sexual bullying'] as $item)
                        <option value="{{ $item }}">
                            {{ $item }}</option>
                    @endforeach
                </select>
            

            
                <div class="col-12 mb-12">
                    <label for="report_content" class="form-label">Report_content</label>
                    <input type="text" class="form-control" id="report_content" name="report_content">
                </div>
            

            <div class="col-12 mb-12">
                <label for="image" class="form-label">Image</label>
                <input type="file" class="form-control" id="image" name="image"
                    accept="image/png,image/jpeg">
            </div>

            
                <div class="col-12 mb-12">
                    <label for="addresed_to" class="form-label">Addresed_to</label>
                    <input type="text" class="form-control" id="addresed_to" name="addresed_to">
                </div>
            

            <div class="col-12 mb-12">
                <label class="form-label">Status</label>
                <select name="status" class="form-select">
                    @foreach (['new report', 'not verified', 'pending', 'unprocessed', 'process', 'finish'] as $item)
                        <option value="{{ $item }}">
                            {{ $item }}</option>
                    @endforeach
                </select>
            

            
                <div class="col-12 mb-12">
                    <label for="reporter_id" class="form-label">Reporter_id</label>
                    <input type="text" class="form-control" id="reporter_id" name="reporter_id">
                </div>
            

            <div class="col-12 mb-12">
                <label class="form-label">Student ID</label>
                <select name="student_id" class="form-select">
                    @foreach ($student_list as $student)
                        <option value="{{ $student_list }}">{{ $student->id}} - {{ $student->user_id}}</option>
                    @endforeach
                </select>
            </div>


            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
             <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>

@endsection