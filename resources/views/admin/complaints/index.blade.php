@extends('app')

@section('content')
    <div class="container">
        <h1>Data Pengaduan</h1>
        <p>{{ $complaint_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Victim_name</th>
                    <th>Incident_date</th>
                    <th>Location</th>
                    <th>Type_bullying</th>
                    <th>Image</th>
                    <th>Addresed_to</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($complaint_list as $complaint)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $complaint->victim_name}}</td>
                        <td>{{ $complaint->incident_date }}</td>
                        <td>{{ $complaint->location }}</td>
                        <td>{{ $complaint->type_bullying }}</td>
                        <td>{{ $complaint->image }}</td>
                        <td>{{ $complaint->addresed_to }}</td>
                        <td>{{ $complaint->status }}</td>
                        <td>
                            <a href="/admin/complaints/{{ $complaint->id }}" class="btn btn-info">Detail</a>
                            <a href="#" class="btn btn-danger" data-bs-toggle="modal"
                                data-bs-target="#modal-{{ $complaint->id }}">Delete</a>
                        </td>
                    </tr>
                    <div class="modal fade" id="modal-{{ $complaint->id }}" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Konfirmasi</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                </div> 
                                <div class="modal-body">
                                    <p>Complaint dengan ID {{ $complaint->id }} akan dihapus.</p>
                                    <p>Lanjutkan?</p>
                                </div>
                                <div class="modal-footer">
                                    <form action="/admin/complaints/{{ $complaint->id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                        <button type="button" class="btn btn-secondary"
                                            data-bs-dismiss="modal">Batal</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
        <a href="/admin/complaints/create" class="btn btn-success">Add New</a>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error}}</p>
            @endforeach
        @endif
    </div>
@endsection
