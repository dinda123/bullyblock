@extends('app')

@section('content')
    <div class="container">
        <h1>Data User</h1>
        <p>{{ $user_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Username</th>
                    <th>Phone</th>
                    <th>Level</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($user_list as $user)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->phone }}</td>
                        <td>{{ $user->level }}</td>
                        <td>
                            <a href="/admin/users/{{ $user->id }}" class="btn btn-info">Detail</a>
                            <a href="#" class="btn btn-danger" data-bs-toggle="modal"
                                data-bs-target="#modal-{{ $user->id }}">Delete</a>
                        </td>
                    </tr>
                    <div class="modal fade" id="modal-{{ $user->id }}" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Konfirmasi</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                </div> 
                                <div class="modal-body">
                                    <p>User dengan ID {{ $user->id }} akan dihapus.</p>
                                    <p>Lanjutkan?</p>
                                </div>
                                <div class="modal-footer">
                                    <form action="/admin/user/{{ $user->id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                        <button type="button" class="btn btn-secondary"
                                            data-bs-dismiss="modal">Batal</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
        <a href="/admin/users/create" class="btn btn-success">Add New</a>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error}}</p>
            @endforeach
        @endif
    </div>
@endsection

