<html>

<head>
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
</head>

<body>
    <div class="container">
        <h1 style="text-align: center">BullyBlock!</h1>
        <div class="row mt-5">
            <div class="col-4 mx-auto mt-5 p-3 bg-light border rounded">
                <h1 class="text-center">Login</h1>
                <form action="/login" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-12 mb-3">
                            <label for="username" class="form-label">Username</label>
                            <input type="text" class="form-control" id="username" name="username">
                        </div>
                        <div class="col-12 mb-3">
                            <label for="password" class="form-label">Password</label>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                        <p>Lupa password? klik disini!</p>
                        <button type="submit" class="btn btn-primary">Login</button>

                    </div>

                </form>
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <p class="text-danger">{{ $error }}</p>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    <script src="{{ mix('/js/app.js') }}"></script>
</body>

</html>
