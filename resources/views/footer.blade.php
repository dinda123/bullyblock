<!DOCTYPE html>
<html lang="en">
  <head>
    <title>BULLYBLOCK!</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width", initial-scale=1">
    <link rel="stylesheet" href="style.css" />
    <link
      rel="stylesheet"
      type="text/css"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
    />
  </head>
  <body>
    <footer class="footer">
      <div class="container">
        <div class="row">
          <div class="footer-col">
            <h4>About us</h4>
            <ul>
              <li><a href="/home">Home</a></li>
              <li><a href="/aboutus">AboutUs</a></li>
              <li><a href="/bullyinginformation">BullyingInformation</a></li>
              <li><a href="#">Our service</a></li>
            </ul>
          </div>
          <div class="footer-col">
            <h4>BullyingInformation</h4>
            <ul>
              <li><a href="/bullyinginformation">Type Bullying</a></li>
              <li><a href="/efekbullying">Efek Bullying</a></li>
              <li><a href="/sanksibullying">Sanksi Bullying</a></li>
            </ul>
          </div>
          <div class="footer-col">
            <h4>Our Service</h4>
            <ul>
              <li><a href="/bullyinginformation">Edukasi Bullying</a></li>
              <li><a href="#">Pelaporan Bullying</a></li>
              <li><a href="/contactus">Contact Us</a></li>
            </ul>
          </div>
          <div class="footer-col">
            <h4>Follow us on</h4>
            <div class="social-media">
              <a href="#"><i class="fab fa-facebook"></i></a>
              <a href="#"><i class="fab fa-instagram"></i></a>
              <a href="#"><i class="fab fa-twitter"></i></a>
              <a href="#"><i class="fab fa-youtube"></i></a>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <div class="fixed-footer">
      <div class="container">GRACIA BULLYBLOCK</div>
    </div>
  </body>
</html>