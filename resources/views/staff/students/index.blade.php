@extends('app')

@section('content')
    <div class="container">
        <h1>Data Siswa</h1>
        <p>{{ $student_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nis</th>
                    <th>Class</th>
                    <th>Gender</th>
                    <th>User_id</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($student_list as $student)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $student->nis }}</td>
                        <td>{{ $student->class }}</td>
                        <td>{{ $student->gender }}</td>
                        <td>{{ $student->user_id }}</td>
                        <td>
                            <a href="/staff/students/{{ $student->id }}" class="btn btn-info">Detail</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
