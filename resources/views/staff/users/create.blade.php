@extends('app')

@section('content')
    <div class="container">
        <h1>Add new user</h1>
        <form action="/admin/users" method="POST">
            @csrf
            <div class="row flex-column">
                <div class="col-12 mb-12">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>

                <div class="col-12 mb-12">
                    <label for="username" class="form-label">Username</label>
                    <input type="text" class="form-control" id="username" name="username">
                </div>


                <div class="col-12 mb-12">
                    <label for="password" class="form-label">Password</label>
                    <input type="text" class="form-control" id="password" name="password">
                </div>



                <div class="col-12 mb-12">
                    <label for="phone" class="form-label">Phone</label>
                    <input type="text" class="form-control" id="phone" name="phone">
                </div>


                <div class="col-12 mb-12">
                    <label class="form-label">Level</label>
                    <select name="level" class="form-select">
                        @foreach (['admin', 'staff'] as $item)
                            <option value="{{ $item }}">
                                {{ $item }}</option>
                        @endforeach
                    </select>
                </div>


            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
