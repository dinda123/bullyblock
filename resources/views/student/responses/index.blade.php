@extends('app')

@section('content')
    <div class="container">
        <p {{ $response_list->links() }}></p>
        <h1>Data Tanggapan</h1>
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Complaint_id</th>
                    <th>Response_date</th>
                    <th>Response</th>
                    <th>Staff_id</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($response_list as $response)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $response->complaint_id }}</td>
                        <td>{{ $response->response_date }}</td>
                        <td>{{ $response->response }}</td>
                        <td>{{ $response->staff_id }}</td>
                        <td>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
