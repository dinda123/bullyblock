@extends('app')

@section('content')
    <div class="container">
        <h1>Detail complaint</h1>
        <form action="/student/complaints/{{ $complaint->id }}" method="POST">
            @csrf
            @method('PATCH')

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="victim_name" class="form-label">Victim_name</label>
                    <input type="text" class="form-control" id="victim_name" name="victim_name" value="{{ $complaint->victim_name }}">
                </div>
            </div>


            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="incident_date" class="form-label">Incident_date</label>
                    <input type="text" class="form-control" id="incident_date" name="incident_date" value="{{ $complaint->incident_date }}">
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="location" class="form-label">Location</label>
                    <input type="text" class="form-control" id="location" name="location" value="{{ $complaint->location }}">
                </div>
            </div>

            <div class="col-3 mb-3">
                <label class="form-label">Type_bullying</label>
                <select name="type_bullying" class="form-select">
                    @foreach (['verbal bullying', 'physical bullying', 'cyber bullying', 'social bullying', 'sexual bullying'] as $item)
                        <option value="{{ $item }}" {{ $complaint->type_bullying == $item ? 'selected' : '' }}>
                            {{ $item }}</option>
                    @endforeach
                </select>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="report_content" class="form-label">Report_content</label>
                    <input type="text" class="form-control" id="report_content" name="report_content" value="{{ $complaint->report_content }}">
                </div>
            </div>

            <div class="col-3 mb-3">
                <label for="image" class="form-label">Image</label>
                <input type="file" class="form-control" id="image" name="image"
                    accept="image/png,image/jpeg">
                <img src="{{ asset('storage/' . $complaint->image) }}" style="width: 300px">
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="addresed_to" class="form-label">Addresed_to</label>
                    <input type="text" class="form-control" id="addresed_to" name="addresed_to" value="{{ $complaint->addresed_to }}">
                </div>
            </div>

            <div class="col-3 mb-3">
                <label class="form-label">Status</label>
                <select name="status" class="form-select">
                    @foreach (['new report', 'not verified', 'pending', 'unprocessed', 'process', 'finish'] as $item)
                        <option value="{{ $item }}" {{ $complaint->status == $item ? 'selected' : '' }}>
                            {{ $item }}</option>
                    @endforeach
                </select>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="reporter_id" class="form-label">Reporter_id</label>
                    <input type="text" class="form-control" id="reporter_id" name="reporter_id" value="{{ $complaint->reporter_id }}">
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
    </div>
@endsection