@extends('app')

@section('content')
    <html>

    <head>
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    </head>

    <body>
        <div class="container">
            <h1 style="text-align: center" id="kata-awal">Form Lapor Bullying</h1>
            <form action="/student/complaints" method="POST" enctype="multipart/form-data">
                <div class="row mt-5">
                    <div class="col-4 mx-auto p-3 mb-5 bg-light border rounded" id="login-kotak">
                        <form action="/student/complaints" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row flex-column">
                                <div class="col-3 mb-3">
                                    <label for="victim_name" class="form-label">Victim_name</label>
                                    <input type="text" class="form-control" id="victim_name" name="victim_name">
                                </div>
                            </div>

                            <div class="row flex-column">
                                <div class="col-3 mb-3">
                                    <label for="incident_date" class="form-label">Incident_date</label>
                                    <input type="date" class="form-control" id="incident_date" name="incident_date">
                                </div>
                            </div>

                            <div class="row flex-column">
                                <div class="col-3 mb-3">
                                    <label for="location" class="form-label">Location</label>
                                    <input type="text" class="form-control" id="location" name="location">
                                </div>
                            </div>

                            <div class="col-3 mb-3">
                                <label class="form-label">Type_bullying</label>
                                <select name="type_bullying" class="form-select">
                                    @foreach (['verbal bullying', 'physical bullying', 'cyber bullying', 'social bullying', 'sexual bullying'] as $item)
                                        <option value="{{ $item }}">
                                            {{ $item }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="row flex-column">
                                <div class="col-3 mb-3">
                                    <label for="report_content" class="form-label">Report_content</label>
                                    <input type="text" class="form-control" id="report_content" name="report_content">
                                </div>
                            </div>
                
                            <div class="col-3 mb-3">
                                <label for="image" class="form-label">Image</label>
                                <input type="file" class="form-control" id="image" name="image"
                                    accept="image/png,image/jpeg">
                            </div>
                
                            <div class="row flex-column">
                                <div class="col-3 mb-3">
                                    <label for="addresed_to" class="form-label">Addresed_to</label>
                                    <input type="text" class="form-control" id="addresed_to" name="addresed_to">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Kirim</button>
                        </form>
                        @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                <p class="text-danger">{{ $error }}</p>
                            @endforeach
                        @endif
                    </div>
                </div>
        </div>
        </div>
        <script src="{{ mix('/js/app.js') }}"></script>
    </body>

    </html>
@endsection
