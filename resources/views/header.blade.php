<?php
    $level = auth()->user()->level;
?>

<header class="p-0 mb-0 border-bottom d-print-none" id="navigation-bar">
    <div class="container-fluid d-flex align-items-center">
        <a href="#" class="d-flex align-items-center m-3 text-decoration-none">
            <img src="/img/Asset 2.png" style="width: 150px">
            <span class="fs-3 ms-2" style="color:secondar"></span>
        </a>
        <ul class="nav" style="margin-left: 20px">
            <li><a href="/home" class="nav-link">Home</a></li>
            <li><a href="/aboutus" class="nav-link">AboutUs</a></li>
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="me-3 nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        BullyingInformation
                    </a>
                    <ul class="dropdown-menu dropdown-menu-dark">
                        <li><a class="dropdown-item" href="/bullyinginformation">Type Bullying</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item" href="/efekbullying">Efek Bullying</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item" href="/sanksibullying">Sanksi bullying</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        MakeAReport
                    </a>
                    <ul class="dropdown-menu dropdown-menu-dark">
                        <li><a class="dropdown-item" href="/student/complaints/create">Pengaduan Onlline</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item" href="/bullyingisreal">BullyingIsReal</a></li>
                    </ul>
                </li>
            </ul>
            <li><a href="contactus" class="me-5 nav-link">ContactUs</a></li>
            <ul class="nav d-flex mb-auto">
                @if($level !='student')
                    <li><a href="/{{ $level }}/users" class="nav-link">User</a></li>
                    <li><a href="/{{ $level }}/students" class="nav-link">Student</a></li>
                @endif
                <li><a href="/{{ $level }}/complaints" class="nav-link">Complaint</a></li>
                <li><a href="/{{ $level }}/responses" class="nav-link">Response</a></li>
            </ul>
            <div class="dropdown me-auto">
                <a href="#" class="dropdown-toggle text-decoration-none" data-bs-toggle="dropdown">
                    <i class="orang bi bi-person-circle" style="font-size: 30px"></i>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="dropdown-item">Hello!, {{ auth()->user()->username}}</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a href="#" class="dropdown-item">Profil</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a href="log out" class="dropdown-item">Laporan saya</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a href="/logout" class="dropdown-item">Log out</a></li>
                </ul>
            </div>
    </div>
</header>
