<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function show()
    {
        return view('login');
    }

    public function check(Request $request)
    {
        $data = $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        if (Auth::attempt($data)) {
            $request->session()->regenerate();

            $user = Auth::user();
            if ($user->level =='admin') {
                return redirect('/home');
            } else if ($user->level == 'staff') {
                return redirect('/home');
            } else if ($user->level == 'student') {
                return redirect('/home');
            }
    }

    return back()->withErrors([
        'msg' => 'Username or password invalid'
    ]);
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/login');
    }
}